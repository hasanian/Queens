package chess.queens;

import java.util.List;

import chess.queens.DataStructure.Column;

public class Util {

	public class Position {
		public int x;
		public int y;

		public Position(int x, int y)// ctor
		{
			this.x = x;
			this.y = y;
		}

		public Position(int lienarPos)// ctor
		{
			this.x = getXyPos(lienarPos).x;
			this.y = getXyPos(lienarPos).y;
		}

		// convert x and y position to linear position
		public int getLinearPos(int x, int y) {
			return (BoardFrame.n * y + x);
		}

		public int getLinearPos() {
			return (BoardFrame.n * this.y + this.x);
		}

		// convert linear position to x and y position
		public Position getXyPos(int linearPos) {
			Position position = new Position(linearPos % BoardFrame.n,
					linearPos / BoardFrame.n);

			return position;
		}
	}

	public void removeGaurdedDomains(List<Column> columns, Position position) {

		for (int i = 0; i < columns.size(); i++) {

			for (int j = 0; j < columns.get(i).domain.size(); j++) {
				if ((columns.get(i).index == position.x)//Horizontal axis
						|| (columns.get(i).domain.get(j) == position.y)//Vertical axis
						|| ((columns.get(i).index - columns.get(i).domain.get(j)) == (position.x - position.y))
						|| ((columns.get(i).index + columns.get(i).domain.get(j)) == (position.x + position.y))) {//diagonal axis

					columns.get(i).domain.remove(j);//remove from i'th column's domain
					j--;
				}
			}

		}

	}
}

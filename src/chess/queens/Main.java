package chess.queens;

import javax.swing.JOptionPane;

import chess.queens.DataStructure.Column;
import chess.queens.DataStructure.Node;

public class Main {

	public static void main(String[] args) {
		
	
		JOptionPane dialog = new JOptionPane();
		String sleepTime = dialog.showInputDialog(null, "Enter sleep time");
		

		// show main from
		BoardFrame boardFrame = new BoardFrame();
		boardFrame.setVisible(true);
		
		try {
			boardFrame.sleepTime = Integer.parseInt(sleepTime);
		} catch (NumberFormatException e) {
			e.printStackTrace();
		}
		
		Util util = new Util();

		DataStructure dataStructure = new DataStructure();
		Node currentNode = dataStructure.new Node();

		currentNode.parent = null;// set parent to null because this is root node

		for (int i = 0; i < boardFrame.n; i++) {
			Column column = dataStructure.new Column(i);
			for (int j = 0; j < boardFrame.n; j++) {
				column.domain.add(j);
			}

			currentNode.unassignedColumns.add(column);
		}

		boolean isFailed = false;
		while (true) {
			if (currentNode.unassignedColumns.size() > 0) {
				Column column = currentNode.unassignedColumns.get(0);
				if (column.domain.size() == 0) {					
					// all possible state is tested and and answer is not fined
					if (currentNode.parent == null)
						{
						isFailed = true;
						break;
					}

					// backtrack
					currentNode = currentNode.parent;
					boardFrame.showQueens(currentNode);
					continue;
				}

				column.row = column.domain.get(0);
				column.domain.remove(0);

				Node childNode = dataStructure.new Node();

				// configure childNode
				childNode.parent = currentNode;
				childNode.assignedColumns = dataStructure
						.cloneColunms(currentNode.assignedColumns);
				childNode.unassignedColumns = dataStructure
						.cloneColunms(currentNode.unassignedColumns);

				// if column assigned correctly remove it from unasigendColumns
				// and add to assigendColumns
				childNode.unassignedColumns.remove(0);
				childNode.assignedColumns.add(column);

				util.removeGaurdedDomains(childNode.unassignedColumns,
						util.new Position(column.index, column.row));

				currentNode = childNode;

				boardFrame.showQueens(currentNode);

			} else {
				break;
			}
		}

		if(isFailed)
		{
			System.out.print("failed ...");
		}
		else
		{
			while(currentNode != null)
			{
				for(int i=0 ;i<BoardFrame.n;i++)
				{
					for(int j=0;j<BoardFrame.n;j++)
					{
						try {
							System.out.print(currentNode.assignedColumns.get(j).row == i ? "x":"0");
						} catch (Exception e) {
							System.out.print("0");
						}
					}
					System.out.println("");
				}
				currentNode = currentNode.parent;
				System.out.println("========");
			}
		}
		
		System.out.print("finish");
	}

}

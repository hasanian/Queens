package chess.queens;

import java.util.ArrayList;
import java.util.List;

public class DataStructure {

	public class Node {
		// ctor
		Node() 
		{
			unassignedColumns = new ArrayList<>();
			assignedColumns = new ArrayList<>();
		}

		// ctor
		Node(List<Column> asigendColumns, List<Column> unasigendColumns) {
			this.assignedColumns = asigendColumns;
			this.unassignedColumns = unasigendColumns;
		}

		// points to parent node
		Node parent;
		// childs are next possible states
		List<Node> childs;

		List<Column> assignedColumns;
		List<Column> unassignedColumns;
	}

	/*
	 * we need n (in this case n = 8) count of Column , each Column models to
	 * one column of chess in some references its called X
	 */
	public class Column {
		Column(int index) {
			domain = new ArrayList<>();
			this.index = index;
			this.row = -1;
		}

		Column(List<Integer> domain , int index ) {
			this.domain = domain;
			this.index = index;
			this.row = -1;
		}
		
		int index;

		List<Integer> domain;

		int row;// in some references its called c
	}
	
	public List<Column> cloneColunms(List<Column> columns)
	{
		List<Column> clone = new ArrayList<>();
		
		for(Column column: columns)
		{
			Column temp = new Column(column.index);
			temp.row = column.row;
			
			for(int element : column.domain)
			{
				temp.domain.add(element);
			}
			
			clone.add(temp);
		}
		
		
		return clone;
	}

}

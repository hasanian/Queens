package chess.queens;
import java.awt.Color;
import java.awt.EventQueue;
import java.awt.GridLayout;
import java.awt.Image;
import java.awt.Rectangle;
import java.util.HashMap;

import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;

import chess.queens.DataStructure.Column;
import chess.queens.DataStructure.Node;
import chess.queens.Util.Position;


public class BoardFrame extends JFrame{

	public static int n = 8;
	JLabel[] BoardItems;
	public int sleepTime = 0;

	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
//					BoardFrame frame = new BoardFrame();
//					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}
	

	/**
	 * Create the frame.
	 */
	public BoardFrame() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 800, 600);
		JPanel chessBoard = new JPanel();
		chessBoard.setLayout(new GridLayout(n, n, 0, 0));
		setContentPane(chessBoard);
		
		BoardItems = new JLabel[n*n];
		
		//paints chess 
		for(int i=0;i<BoardItems.length;i++)
		{
			
			BoardItems[i] = new JLabel("");//writes linear position in each cell
			//fill black and white decussate
			if((i/n)%2 == 0)
				BoardItems[i].setBackground(i%2==0?Color.black:Color.WHITE);
			else
				BoardItems[i].setBackground(i%2==0?Color.WHITE:Color.black);
			
			BoardItems[i].setOpaque(true);
			BoardItems[i].setBounds(new Rectangle(this.WIDTH/n, this.WIDTH/n));
			chessBoard.add(BoardItems[i]);//add cell to form (panel)
		}		
	}

	public void showQueens(HashMap<Integer, Integer> queensPositions)
	{
		for(JLabel boardItem : BoardItems)
		{
			boardItem.setIcon(null);
		}
		Image queenImage = new ImageIcon(BoardFrame.class.getResource("/chess/queens/q.png")).getImage();
		Image resizedImage = queenImage.getScaledInstance(BoardItems[0].getWidth(), BoardItems[0].getHeight(), 0);
		ImageIcon queenIcon = new ImageIcon(resizedImage);		
		
		Util util = new Util();
		Position position = util.new Position(0);
		
		for(int xAxis : queensPositions.keySet())
		{
			BoardItems[position.getLinearPos(xAxis, queensPositions.get(xAxis))].setIcon(queenIcon);
		}
		
	}

	public void showQueens(Node node)
	{
		HashMap<Integer, Integer> queensPositions = new HashMap<Integer, Integer>();
		
		for(Column column : node.assignedColumns)
		{
			queensPositions.put(column.index, column.row);
		}
		
		showQueens(queensPositions);
		
		try {
			Thread.sleep(sleepTime);
		} catch (NumberFormatException | InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
